import React from "react";
import Button from "../components/Button";
import Main from "../components/Main";
import { student } from "../Data";

var today = new Date();

today.setDate(today.getDate()-7);
var filterData = student.filter((d) => {
  return new Date(d.date).getTime() >= today.getTime();
});
console.log(filterData);

today.setDate(today.getDate() - 30);
var thirtydayFilterData = student.filter((d) => {
  return new Date(d.date).getTime() >= today.getTime();
});
console.log(thirtydayFilterData);

var to = new Date();
to.setDate(today.getDate() - 60);
var sixtydayFilterData = student.filter((d) => {
  return new Date(d.date).getTime() >= to.getTime();
});
console.log(sixtydayFilterData);

class Home extends React.Component {
  state = {
    user: "",
    userS: "",
    userR: "",
  };
  Total = () => {
    var Total = student.length;
    this.setState({ user: Total });
  };
  selecetdeTotal = () => {
    var selected = student.filter((info) => info.status === "selected");

    this.setState({ userS: selected.length });
  };
  rejectedTotal = () => {
    var rejected = student.filter((info) => info.status === "rejected");
    this.setState({ userR: rejected.length });
  };
  sevendaysTotal = () => {
    var datecount = filterData;
    this.setState({ user: datecount.length });
  };

  sevendaysSelected = () => {
    var datecount = filterData.filter((info) => info.status === "selected");

    this.setState({ userS: datecount.length });
  };
  sevendaysRejected = () => {
    var datecount = filterData.filter((info) => info.status === "rejected");

    this.setState({ userR: datecount.length });
  };

  thirtyDaysTotal = () => {
    var datecount = thirtydayFilterData;

    this.setState({ user: datecount.length });
  };
  thirtyDaysSelected = () => {
    var datecount = thirtydayFilterData.filter(
      (info) => info.status === "selected"
    );

    this.setState({ userS: datecount.length });
  };

  thirtyDaysRejected = () => {
    var datecount = thirtydayFilterData.filter(
      (info) => info.status === "rejected"
    );

    this.setState({ userR: datecount.length });
  };

  sixtyDaysTotal = () => {
    var datecount = sixtydayFilterData;

    this.setState({ user: datecount.length });
  };

  sixtyDaysSelected = () => {
    var datecount = sixtydayFilterData.filter(
      (info) => info.status === "selected"
    );

    this.setState({ userS: datecount.length });
  };

  sixtyDaysRejected = () => {
    var datecount = sixtydayFilterData.filter(
      (info) => info.status === "rejected"
    );

    this.setState({ userR: datecount.length });
  };

  render() {
    return (
      <React.Fragment>
        <div className="reg">
          <div className="grid-container">
            <header className="header">
              <Button
                selectedTotal={this.selecetdeTotal}
                rejectedTotal={this.rejectedTotal}
                total={this.Total}
                sevendaysTotal={this.sevendaysTotal}
                sevendaysSelected={this.sevendaysSelected}
                sevendaysRejected={this.sevendaysRejected}
                thirtyDaysTotal={this.thirtyDaysTotal}
                thirtyDaysSelected={this.thirtyDaysSelected}
                thirtyDaysRejected={this.thirtyDaysRejected}
                sixtyDaysTotal={this.sixtyDaysTotal}
                sixtyDaysSelected={this.sixtyDaysSelected}
                sixtyDaysRejected={this.sixtyDaysRejected}
              />
            </header>
            <main className="main">
              <Main
                user={this.state.user}
                userS={this.state.userS}
                userR={this.state.userR}
              />
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Home;
