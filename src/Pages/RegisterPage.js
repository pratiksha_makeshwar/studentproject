import React, { Component } from "react";
import {
  Form,
  FormGroup,
  FormControl,
  FormLabel,
  Button,
} from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { addNewStud } from "../allactions";
import Table from '../components/Table';


class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      status: "",
      date: "",
      formdata: [],
    };

    // This binding is necessary to make `this` work in the callback
    this.handleSubmit = this.handleSubmit.bind(this);
    //this.sendData= this.sendData.bind(this);
    
  }
  handleSubmit(e) {
    e.preventDefault();
    let formdata = [...this.state.formdata] 
    formdata.push ( {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      status: this.state.status,
      date: this.state.date,
    });

    
    //this.props.addNewStudent(formdata);
    
    //this.props.func(formdata)
    
    

    this.setState({
      firstname: "",
      lastname: "",
      status: "",
      date: "",
      formdata,
    });
  }
  render() {
    console.log("submitted", this.state.formdata);
    return (
      <div className="col-md-4 offset-md-4">
        <h2 style={{ textAlign: "center" }}>Enter Employee Details</h2>
        <hr />
        <Form>
          <FormGroup>
            <FormLabel>Firstname</FormLabel>
            <FormControl
              type="text"
              name="firstname"
              placeholder="Firstname"
              onChange={(e) => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              value={this.state.firstname}
            />
          </FormGroup>

          <FormGroup>
            <FormLabel>Lastname</FormLabel>
            <FormControl
              type="text"
              name="lastname"
              placeholder="Lastname"
              onChange={(e) => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              value={this.state.lastname}
            />
          </FormGroup>

          <FormGroup>
            <FormLabel>Status</FormLabel>
            <FormControl
              type="text"
              name="status"
              placeholder="Status"
              onChange={(e) => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              value={this.state.status}
            />
          </FormGroup>

          <FormGroup>
            <FormLabel>Date</FormLabel>
            <FormControl
              type="date"
              name="date"
              placeholder="Date"
              onChange={(e) => {
                this.setState({ [e.target.name]: e.target.value });
              }}
              value={this.state.date}
            />
          </FormGroup>

          <Button className="mt-3" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Form>
        <Table items={this.state.formdata}/>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addNewStud }, dispatch);
}
export default connect(null, mapDispatchToProps)(RegisterPage);
