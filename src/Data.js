export const student = [
  {
    firstname: "gaurav",
    lastname: "mane",
    status: "selected",
    date: "2021-12-25",
  },
  {
    firstname: "pratik",
    lastname: "patil",
    status: "selected",
    date: "2022-02-23",
  },
  {
    firstname: "sakshi",
    lastname: "kale",
    status: "selected",
    date: "2021-12-30",
  },
  {
    firstname: "pooja",
    lastname: "doke",
    status: "selected",
    date: "2022-01-25",
  },
  {
    firstname: "manali",
    lastname: "shinde",
    status: "rejected",
    date: "2022-02-18",
  },
  {
    firstname: "kran",
    lastname: "gaikwad",
    status: "rejected",
    date: "2021-11-20",
  },
  {
    firstname: "tejal",
    lastname: "kamble",
    status: "selected",
    date: "2021-11-30",
  },
  {
    firstname: "shruti",
    lastname: "bharane",
    status: "selected",
    date: "2021-12-26",
  },
  {
    firstname: "vedika",
    lastname: "polekar",
    status: "rejected",
    date: "2021-04-21",
  },
  {
    firstname: "pranali",
    lastname: "bodke",
    status: "selected",
    date: "2021-01-10",
  },
  {
    firstname: "nikita",
    lastname: "mohite",
    status: "rejected",
    date: "2022-01-14",
  },

];

