export function addNewStud(payload) {
  const action = {
    type: "ADD_NEW_STUD",
    payload,
  };
  return action;
}
