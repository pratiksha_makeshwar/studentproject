import React from "react";
import "./Button.css";

function Button({selectedTotal,rejectedTotal,total,sevendaysTotal,sevendaysSelected,sevendaysRejected,
  thirtyDaysTotal, thirtyDaysSelected,thirtyDaysRejected,sixtyDaysTotal,sixtyDaysSelected,sixtyDaysRejected
}) {
  return (
    <React.Fragment>
      <ul>
        <button className="button" onClick={() => {sixtyDaysTotal();sixtyDaysSelected();sixtyDaysRejected()}} >60 Days</button>
        <button className="button" onClick={() => {thirtyDaysTotal(); thirtyDaysSelected();thirtyDaysRejected()}} >30 Days </button>
        <button className="button" onClick={()=>{sevendaysTotal();sevendaysSelected();sevendaysRejected()}} >7 Days</button>
        <button className="button" onClick={() => {selectedTotal(); rejectedTotal();total()}} >All</button>
      </ul>
    </React.Fragment>
  );
}
export default Button;

