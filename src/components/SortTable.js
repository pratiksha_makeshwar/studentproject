import React from 'react'
import { connect } from "react-redux";

function SortTable(props) {
    const {student} = props.Data;
    const [sortConfig, setSortConfig] = React.useState(null); 
    let sortedProducts = [...student];
    if(sortConfig !== null){
    sortedProducts.sort((a,b)=> {
        if(a[sortConfig.key] < b[sortConfig.key]){
            return sortConfig.direction === 'ascending' ? -1 : 1;
        }
        if(a[sortConfig.key] > b[sortConfig.key]){
            return sortConfig.direction === 'ascending' ? 1 : -1;
        }
        return 0;
    });
}

const requestSort = key => {
    let direction = 'ascending';
    if( sortConfig.key === key &&  sortConfig.direction === 'ascending'){
        direction = 'descending';
    }
    setSortConfig({key, direction})
}
  return (
    <div>
    
    </div>
  )
}

function mapStateToProps(state) {
    return {
      Data: state,
    };
  }
export default connect(mapStateToProps, null)(SortTable);