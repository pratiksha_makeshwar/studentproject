import React, { Component } from 'react';
import { connect } from "react-redux";

 class Sort extends Component {
     state={
         stud : this.props.Data
     }

    compareBy(key) {
        return function (a, b) {
          if (a[key] > b[key]) {
            return 1;
          }
          if (b[key] > a[key]) {
            return -1;
          }
          return 0;
        };
      }
    
      sortBy(key) {
        let arrayCopy = [...this.state.stud];
        arrayCopy.sort(this.compareBy(key));
        this.setState({ stud: arrayCopy });
      }
  render() {
    return (
      <div>Sort</div>
    )
  }
}
function mapStateToProps(state) {
    return {
      Data: state,
    };
  }

export default connect(mapStateToProps, null)(Sort);