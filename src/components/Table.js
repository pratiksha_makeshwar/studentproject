import React from 'react'

function Table(props) {
    const items = props.items
  return (
    <div className='mt-5'>

      <table className='table table-bordered'>
      <thead>
         <tr>
             <th>#</th>
             <th>First Name</th>
             <th>Last Name</th>
             <th>Status</th>
             <th>Date</th>
         </tr>
         </thead>
         <tbody>
              {items.map((info, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{info.firstname}</td>
                    <td>{info.lastname}</td>
                    <td>{info.status}</td>
                    <td>{info.date}</td>
                  </tr>
                );
              })}
            </tbody>
     </table> 

    </div>
  )
}

export default Table;