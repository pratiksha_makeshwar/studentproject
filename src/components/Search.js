import React, { useState } from 'react'
import {connect} from 'react-redux'

function Search(props) {
    const [searchInput, setSearchInput] = useState("");
    const [stud, setStud] = useState(props.Data)

  function  handleChange (event){
        setSearchInput( event.target.value, () =>{
          globalSearch();
        })
      };
      function globalSearch () {
        
        if(searchInput){
          let filterData = stud.filter(value => {
            return(
              value.firstname.toLowerCase().includes(searchInput.toLowerCase()) ||
              value.lastname.toLowerCase().includes(searchInput.toLowerCase()) 
              
            );
          });
          setStud(filterData);
        }
      }
  return (
    <div>
         <input
            
            type="search"
            placeholder="search"
            value={searchInput || ""}
            onChange={handleChange}
            
           />
    </div>
  )
}
function mapStateToProps(state) {
    return {
      Data: state,
    };
  } 
export default connect(mapStateToProps, null)(Search);