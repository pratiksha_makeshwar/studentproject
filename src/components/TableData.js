import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { connect } from "react-redux";
import { DropdownButton } from "react-bootstrap";
import "./Button.css";
import DropDownList from "./DropDownList";
//import RegisterPage from "../Pages/RegisterPage";





class TableData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stud: [...this.props.Data],
      searchInput: "",
    };
   // this.compareBy.bind(this);
    //this.sortBy.bind(this);
  }

  addRows = (data) => {
    const updatedStudentData = [...this.state.stud];
    updatedStudentData.push(data);
    this.setState({ stud: updatedStudentData });
  };

  handleSelect = (e) => {
    let finalData = this.props.Data;
    if (e === "all") {
      this.setState({ stud: this.props.Data });
    } else if (e === "selected") {
      const selected = finalData.filter((info) => info.status === "selected");

      this.setState({ stud: [...selected] });
    } else if (e === "rejected") {
      const rejected = finalData.filter((info) => info.status === "rejected");
      this.setState({ stud: [...rejected] });
    } //this.setState({value: e }
  };

  compareBy(key) {
    return function (a, b) {
      if (a[key] > b[key]) {
        return 1;
      }
      if (b[key] > a[key]) {
        return -1;
      }
      return 0;
    };
  }

  sortBy(key) {
    let arrayCopy = [...this.state.stud];
    arrayCopy.sort(this.compareBy(key));
    this.setState({ stud: arrayCopy });
  }
  handleChange = (event) => {
    this.setState({ searchInput: event.target.value }, () => {
      this.globalSearch();
    });
  };
  globalSearch = () => {
    let { stud, searchInput } = this.state;
    if (searchInput) {
      let filterData = stud.filter((value) => {
        return (
          value.firstname.toLowerCase().includes(searchInput.toLowerCase()) ||
          value.lastname.toLowerCase().includes(searchInput.toLowerCase())
        );
      });
      this.setState({ stud: filterData });
    }
  };

  render() {
    const { stud, searchInput } = this.state;

    return (
      <React.Fragment>
        <div className="col-md-8 offset-md-3">
          <h3 className="d-flex justify-content-center">Student Information</h3>
          <div>
            <br />
            <label>Search: </label>

            <input
              type="search"
              placeholder="search"
              value={searchInput || ""}
              onChange={this.handleChange}
            />
          </div>
          <table className="table table-bordered mt-5">
            <thead>
              <tr>
                <th>#</th>
                <th>
                  <div onClick={() => this.sortBy("firstname")}>First Name</div>
                </th>
                <th>
                  <div onClick={() => this.sortBy("lastname")}>Last Name</div>
                </th>
                <th className="w-25 ">
                  Status
                  <div className="drop">
                    <DropdownButton title="Select" onSelect={this.handleSelect}>
                      <DropDownList />
                    </DropdownButton>
                  </div>
                </th>
                <th>
                  <div onClick={() => this.sortBy("date")}>Date</div>
                </th>
              </tr>
            </thead>
            <tbody>
              {stud.map((info, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{info.firstname}</td>
                    <td>{info.lastname}</td>
                    <td>{info.status}</td>
                    <td>{info.date}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
           {/* <RegisterPage func={this.addRows}/>  */}
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    Data: state,
  };
}
export default connect(mapStateToProps, null)(TableData);
