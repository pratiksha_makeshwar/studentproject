import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";

function Navbar() {
  return (
    <React.Fragment>
      <ul className="par">
        <li>
          <Link to="/" className="navbar-container">
            Home
          </Link>
        </li>
        <li>
          <Link to="/candidate" className="navbar-container">
            Candidate
          </Link>
        </li>
        <li>
          <Link to="/registercandidate" className="navbar-container">
            Register Candidates
          </Link>
        </li>
      </ul>
    </React.Fragment>
  );
}
export default Navbar;
