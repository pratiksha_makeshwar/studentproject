import React from "react";
import { Dropdown } from "react-bootstrap";

function DropDownList() {
  return (
    <div>
      <Dropdown.Item eventKey="all">All</Dropdown.Item>
      <Dropdown.Item eventKey="selected">Selected</Dropdown.Item>
      <Dropdown.Item eventKey="rejected">Rejected</Dropdown.Item>
    </div>
  );
}

export default DropDownList;
