import React from "react";



function Main({user,userS,userR}) {
 
 
  
  return (
    
    <React.Fragment>
    <div className="container">
    <div className="row mt-5">
      <div className=" col input">
      <p className="mt-4">Total Number of Candidates:{user}</p>
      </div>
      <div className="col input" >
      <p className="mt-4">Total Selected Candidates:{userS}</p> 
      </div>
      <div className="col input"> 
      <p className="mt-4">Total Rejected Candidates:{userR}</p>
      </div>
      </div>
      </div>
    </React.Fragment>
  );
}

export default Main;
