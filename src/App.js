import "./App.css";
import Home from "./Pages/Home";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Students from "./Pages/Students";
import Navbar from "./components/Navbar";

import RegisterPage from "./Pages/RegisterPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route exact path="/"  element={<Home />} />
          <Route exact path="/candidate" element={<Students />} />
          <Route exact path="/registercandidate" element={<RegisterPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
